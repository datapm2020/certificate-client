import aiohttp
import asyncio
import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey
from security_helper import certificate_utils
from cryptography.hazmat.primitives import serialization
from aiohttp import web
from environment_settings import client_settings, ca_settings

routes = web.RouteTableDef()


async def run_server(uid):
    @routes.get('/checkclient/')
    async def hello(request):
        return web.Response(text=uid)

    app = web.Application()
    app.add_routes(routes)

    loop = asyncio.get_running_loop()
    loop.create_task(web._run_app(app, port=80))
    await asyncio.sleep(0)


async def check_configs(session, url):
    for value in client_settings.dict().values():
        if value is None:
            async with session.get(url) as resp:
                config = await resp.json()
                for key in config:
                    if getattr(client_settings, key) is None:
                        setattr(client_settings, key, config[key])
            break


def check_private_key():
    if isinstance(client_settings.PRIVATE_KEY, RSAPrivateKey):
        pass
    private_key_path = client_settings.PRIVATE_KEY_FILE
    try:
        with open(private_key_path, 'rb') as pem:
            client_settings.PRIVATE_KEY = serialization.load_pem_private_key(
                pem.read(),
                None,
                default_backend()
            )
    except:
        client_settings.PRIVATE_KEY = certificate_utils.generate_private_key_in_file(
            private_key_path,
            public_exponent=client_settings.PUBLIC_EXPONENT,
            default_bits=client_settings.DEFAULT_BITS
        )
    return client_settings.PRIVATE_KEY


def write_certificates(data, caFilePath, certificateFilePath):
    with open(caFilePath, "wb") as certfile:
        certfile.write(bytes(data['ca_certificate'], 'utf-8'))
    with open(certificateFilePath, "wb") as certfile:
        certfile.write(bytes(data['certificate'], 'utf-8'))


async def main():
    async with aiohttp.ClientSession() as session:
        await check_configs(session, ca_settings.CA_SERVICE_URL+"/certificate-authority/api/v1/certificate/config/")
        private_key = check_private_key()
        csr = certificate_utils.generate_certificate_signing_request(
            private_key,
            country=client_settings.COUNTRY_NAME,
            state=client_settings.STATE_OR_PROVINCE_NAME,
            locality=client_settings.LOCALITY_NAME,
            org=client_settings.ORGANIZATION_NAME,
            hostname=client_settings.COMMON_NAME,
            alt_names=client_settings.SUBJECT_ALT_NAME,
            organizational_unit=client_settings.ORGANIZATIONAL_UNIT
        )
        data = {'signing_request': (csr.public_bytes(serialization.Encoding.PEM)).decode("utf-8")}
        async with session.post(ca_settings.CA_SERVICE_URL+"/certificate-authority/api/v1/certificate/request/", json=data) as resp:
            uid = await resp.text()

        # await run_server(uid)

        async with session.get(ca_settings.CA_SERVICE_URL+"/certificate-authority/api/v1/certificate/"+uid[1:-1]) as resp:
            if 400 <= resp.status <= 499:
                raise NameError("There's a problem with the request: " +resp.text())
            x = await resp.json()
        data = {
            **x,
            'private_key': private_key.private_bytes(
                    encoding=serialization.Encoding.PEM,
                    format=serialization.PrivateFormat.TraditionalOpenSSL,
                    encryption_algorithm=serialization.NoEncryption(),
                ).decode("utf-8")
        }
        write_certificates(data, client_settings.CA_FILE, client_settings.CERTIFICATE_FILE)

if __name__ == "__main__":
    asyncio.run(main())
