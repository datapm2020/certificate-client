import os

from pydantic import validator, BaseSettings
from typing import List, Dict, Any, Optional


class ClientSettings(BaseSettings):
    PUBLIC_EXPONENT: int = None
    DEFAULT_BITS: int = None
    COUNTRY_NAME: str = None
    STATE_OR_PROVINCE_NAME: str = None
    LOCALITY_NAME: str = None
    ORGANIZATION_NAME: str = None

    COMMON_NAME: str
    SUBJECT_ALT_NAME: List
    ORGANIZATIONAL_UNIT: str = "client"

    PRIVATE_KEY: str = None
    PRIVATE_KEY_FILE: str = "/secrets/private-key.pem"

    CA_FILE: str = "/secrets/ca-cert.pem"

    CERTIFICATE_FILE: str = "/secrets/cert.pem"

    ENABLE_MONGODB: bool = False


if os.path.isdir('/secrets'):
    client_settings = ClientSettings(_secrets_dir="/secrets")
else:
    client_settings = ClientSettings()


class CertificateAuthoritySettings(BaseSettings):
    CA_SERVICE_URL: str


ca_settings = CertificateAuthoritySettings()
