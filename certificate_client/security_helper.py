import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID, ExtensionOID, ExtendedKeyUsageOID
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
import ipaddress


class CertificateUtils:

    def generate_private_key(self, **kwargs):
        private_key = rsa.generate_private_key(
            public_exponent=kwargs['public_exponent'], key_size=kwargs['default_bits'], backend=default_backend()
        )
        return private_key

    def generate_private_key_in_file(self, filePath, **kwargs):
        private_key = self.generate_private_key(**kwargs)
        with open(filePath, "wb") as keyfile:
            keyfile.write(
                private_key.private_bytes(
                    encoding=serialization.Encoding.PEM,
                    format=serialization.PrivateFormat.TraditionalOpenSSL,
                    encryption_algorithm=serialization.NoEncryption(),
                )
            )
        return private_key

    def generate_certificate_signing_request(self, private_key, **kwargs):
        subject = x509.Name(
            [
                x509.NameAttribute(NameOID.COUNTRY_NAME, kwargs["country"]),
                x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, kwargs["state"]),
                x509.NameAttribute(NameOID.LOCALITY_NAME, kwargs["locality"]),
                x509.NameAttribute(NameOID.ORGANIZATION_NAME, kwargs["org"]),
                x509.NameAttribute(NameOID.COMMON_NAME, kwargs["hostname"]),
                x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME, kwargs["organizational_unit"])
            ]
        )
        alt_names = []
        for name in kwargs.get("alt_names", []):
            try:
                ip = ipaddress.ip_address(name)
                alt_names.append(x509.IPAddress(ip))
            except:
                alt_names.append(x509.DNSName(name))
        san = x509.SubjectAlternativeName(alt_names)

        if False: # todo doesn't work when the certificate is used for the server but needed for mongodb
            key_usage = x509.KeyUsage(
                key_agreement=False, key_encipherment=False, digital_signature=True, crl_sign=False,
                content_commitment=False, data_encipherment=False, key_cert_sign=False, encipher_only=False,
                decipher_only=False)
            extended_key_usage = x509.ExtendedKeyUsage(usages=[ExtendedKeyUsageOID.CLIENT_AUTH])
            builder = (
                x509.CertificateSigningRequestBuilder()
                .subject_name(subject)
                .add_extension(san, critical=True)
                .add_extension(key_usage, critical=True)
                .add_extension(extended_key_usage, critical=True)
            )
        else:
            builder = (
                x509.CertificateSigningRequestBuilder()
                .subject_name(subject)
                .add_extension(san, critical=True)
            )
        csr = builder.sign(private_key, hashes.SHA256(), default_backend())
        return csr


certificate_utils = CertificateUtils()